function varargout = Trim_Calculator(varargin)
% TRIM_CALCULATOR MATLAB code for Trim_Calculator.fig
%      TRIM_CALCULATOR, by itself, creates a new TRIM_CALCULATOR or raises the existing
%      singleton*.
%
%      H = TRIM_CALCULATOR returns the handle to a new TRIM_CALCULATOR or the handle to
%      the existing singleton*.
%
%      TRIM_CALCULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRIM_CALCULATOR.M with the given input arguments.
%
%      TRIM_CALCULATOR('Property','Value',...) creates a new TRIM_CALCULATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Trim_Calculator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Trim_Calculator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Trim_Calculator

% Last Modified by GUIDE v2.5 08-Oct-2013 11:05:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Trim_Calculator_OpeningFcn, ...
                   'gui_OutputFcn',  @Trim_Calculator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Trim_Calculator is made visible.
function Trim_Calculator_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Trim_Calculator (see VARARGIN)

% Choose default command line output for Trim_Calculator
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Trim_Calculator wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Trim_Calculator_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function vt_edit_Callback(hObject, eventdata, handles)
% hObject    handle to vt_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of vt_edit as text
%        str2double(get(hObject,'String')) returns contents of vt_edit as a double


% --- Executes during object creation, after setting all properties.
function vt_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vt_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alt_edit_Callback(hObject, eventdata, handles)
% hObject    handle to alt_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alt_edit as text
%        str2double(get(hObject,'String')) returns contents of alt_edit as a double


% --- Executes during object creation, after setting all properties.
function alt_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alt_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in trim_calc.
function trim_calc_Callback(hObject, eventdata, handles)
alt_entry = str2double(get(handles.alt_edit,'String'));
vt_entry = str2double(get(handles.vt_edit,'String'));
[Alfa delH Thtl] = trim(alt_entry,vt_entry);
delH = delH / 25;

set(handles.alpha,'String',num2str(Alfa));
set(handles.delH,'String',num2str(delH));
set(handles.thtl,'String',num2str(Thtl));
% hObject    handle to trim_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
close;
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

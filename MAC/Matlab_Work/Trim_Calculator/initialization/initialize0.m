function [aero input] = initialize0
load_const;
%% Set up inputs.
% Define Aerodynamic Parameters
aero.Vt      = 550;              % TAS Velocity (ft/s)
aero.alfa    = 17;               % AoA Degrees
aero.beta    = -21;              % Sideslipt degrees
aero.beta_rad= deg2rad(-21);     % Sideslip radians
aero.phi     = 0;     % Roll angle
aero.th      = 0;      % Pitch Angle
aero.psi     = 0;    % Yaw Angle
aero.P       = deg2rad(-37);     % Roll Rate  (rad/s)
aero.Q       = deg2rad(27);      % Pitch Rate (rad/s)
aero.R       = deg2rad(-17);     % Yaw Rate   (rad/s)
aero.X       =  5* nm2ft;        % X inertial position
aero.Y       = -3* nm2ft;        % Y inertial position
aero.Z       = -30500;           % Z inertial position (pos toward ground)
aero.H       = -1*aero.Z;        % Altitude (ft)
% Initialize altitude dependent variables to 1;
aero.Temp    = 1;                % OAT due to alt
aero.Pres    = 1;                % pressure due to alt
aero.rho     = 1;                % rho due to alt
aero.Vs      = 1;                % Sonic speed due to altitude
% Initialize all other variables
aero.T       = 1;                % Thrust
aero.fax     = 1;
aero.fay     = 1;
aero.faz     = 1;
aero.la      = 1;
aero.ma      = 1;
aero.na      = 1;
aero.fpx     = 1;
aero.fpy     = 0;
aero.fpz     = 0;
aero.lp      = 0;
aero.mp      = 0;
aero.np      = 0;
aero.gx      = 1;
aero.gy      = 1;
aero.gz      = 1;
aero.U = aero.Vt*cosd(aero.alfa)*cosd(aero.beta);
aero.V = aero.Vt*sind(aero.beta);
aero.W = aero.Vt*sind(aero.alfa)*cosd(aero.beta);
% Define Control Input Parameters (Degrees)
input.delH        = -17;      % Horizontal tail deflection (Pos down)
input.delA        = 11;       % Aileron Deflection       (Right down Pos)
input.delR        = 13;       % Rudder Defection          (Left is Pos)
input.delFlap     = 15;       % Flap Defection            (Always Pos)
input.delSpBr     = 9;        % Speedbrake Deflection     (Always Pos)
input.thtl        = 0.61;     % Trottle setting  (100% max)
end
function [aero input] = init_F16
load_const;
%% Set up inputs.
% Define Aerodynamic Parameters
aero.Vt      = 600;              % TAS Velocity (ft/s)
aero.alfa    = 0;               % AoA Degrees
aero.beta    = -0;              % Sideslipt degrees
aero.beta_rad= deg2rad(-0);     % Sideslip radians
aero.phi     = 0;     % Roll angle
aero.th      = 0;      % Pitch Angle
aero.psi     = 0;    % Yaw Angle
aero.P       = deg2rad(0);     % Roll Rate  (rad/s)
aero.Q       = deg2rad(0);      % Pitch Rate (rad/s)
aero.R       = deg2rad(0);     % Yaw Rate   (rad/s)
aero.X       =  0* nm2ft;        % X inertial position
aero.Y       =  0* nm2ft;        % Y inertial position
aero.Z       = -5000;           % Z inertial position (pos toward ground)
aero.H       = -1*aero.Z;        % Altitude (ft)
% Initialize altitude dependent variables to 1;
aero.Temp    = 1;                % OAT due to alt
aero.Pres    = 1;                % pressure due to alt
aero.rho     = 1;                % rho due to alt
aero.Vs      = 1;                % Sonic speed due to altitude
% Initialize all other variables
aero.T       = 1;                % Thrust
aero.fax     = 1;
aero.fay     = 1;
aero.faz     = 1;
aero.la      = 1;
aero.ma      = 1;
aero.na      = 1;
aero.fpx     = 1;
aero.fpy     = 0;
aero.fpz     = 0;
aero.lp      = 0;
aero.mp      = 0;
aero.np      = 0;
aero.gx      = 1;
aero.gy      = 1;
aero.gz      = 1;
aero.U = aero.Vt*cosd(aero.alfa)*cosd(aero.beta);
aero.V = aero.Vt*sind(aero.beta);
aero.W = aero.Vt*sind(aero.alfa)*cosd(aero.beta);
% Define Control Input Parameters (Degrees)
input.delH        = 0;      % Horizontal tail deflection (Pos down)
input.delA        = 0;       % Aileron Deflection       (Right down Pos)
input.delR        = 0;       % Rudder Defection          (Left is Pos)
input.delFlap     = 0;       % Flap Defection            (Always Pos)
input.delSpBr     = 0;        % Speedbrake Deflection     (Always Pos)
input.thtl        = 0.0;     % Trottle setting  (100% max)
end
function [aero ctrlin] = initialize
load_const;
%% Set up inputs.
% Define Aerodynamic Parameters
aero.Vt      = 600;              % TAS Velocity (ft/s)
aero.alfa    = 17;               % AoA Degrees
aero.beta    = 0;              % Sideslipt degrees
aero.beta_rad= 0;     % Sideslip radians
aero.phi     = 0;     % Roll angle
aero.th      = 0;      % Pitch Angle
aero.psi     = 0;    % Yaw Angle
aero.P       = 0;     % Roll Rate  (rad/s)
aero.Q       = 0;      % Pitch Rate (rad/s)
aero.R       = 0;     % Yaw Rate   (rad/s)
aero.X       =  5* nm2ft;        % X inertial position
aero.Y       = -3* nm2ft;        % Y inertial position
aero.Z       = -25000;           % Z inertial position (pos toward ground)
aero.H       = -1*aero.Z;        % Altitude (ft)
% Initialize altitude dependent variables to 1;
aero.Temp    = 1;                % OAT due to alt
aero.Pres    = 1;                % pressure due to alt
aero.rho     = 1;                % rho due to alt
aero.Vs      = 1;                % Sonic speed due to altitude
% Initialize all other variables
aero.T       = 1;                % Thrust
aero.fax     = 1;
aero.fay     = 1;
aero.faz     = 1;
aero.la      = 1;
aero.ma      = 1;
aero.na      = 1;
aero.fpx     = 1;
aero.fpy     = 0;
aero.fpz     = 0;
aero.lp      = 0;
aero.mp      = 0;
aero.np      = 0;
aero.gx      = 1;
aero.gy      = 1;
aero.gz      = 1;
aero.U       = 1;
aero.V       = 1;
aero.W       = 1;
% Define Control Input Parameters (Degrees)
ctrlin.delH        = -17;      % Horizontal tail deflection (Pos down)
ctrlin.delA        = 0;       % Aileron Deflection       (Right down Pos)
ctrlin.delR        = 0;       % Rudder Defection          (Left is Pos)
ctrlin.delFlap     = 0;       % Flap Defection            (Always Pos)
ctrlin.delSpBr     = 0;        % Speedbrake Deflection     (Always Pos)
ctrlin.thtl        = 0.61;     % Trottle setting  (100% max)
end
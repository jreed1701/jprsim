function x_vect = EOM(X,U)
%% Load required constants;
declare_global;
% Update altitude dependent values
altvars = calc_alt_vars(-X(3));
% Calculate Aircraft Forces and Moments
acdata = calc_AC_data(X,U,altvars);
%% Unpack input structures (Makes code cleaner)
[fax fay faz fpx fpy fpz gx gy gz la ma na lp mp np P Q R psi th ...
    phi U V W] = reassign(X,acdata);

% x_d = (cosd(psi)*cosd(th)*U)+ ...
%      ((cosd(psi)*sind(th)*sind(phi))- ...
%       (sind(psi)*cosd(phi)))*V + ...
%      ((cosd(psi)*sind(th)*cosd(phi))+ ...
%       (sind(psi)*sind(phi)))*W;
x_d = (cos(psi)*cos(th)*U)+ ...
     ((cos(psi)*sin(th)*sind(phi))- ...
      (sin(psi)*cos(phi)))*V + ...
     ((cos(psi)*sin(th)*cosd(phi))+ ...
      (sin(psi)*sin(phi)))*W;

% y_d = (sind(psi)*cosd(th)*U)+ ...
%      ((sind(psi)*sind(th)*sind(phi))+ ...
%       (cosd(psi)*cosd(phi)))*V + ...
%      ((sind(psi)*sind(th)*cosd(phi))- ...
%       (cosd(psi)*sind(phi)))*W;
y_d = (sin(psi)*cos(th)*U)+ ...
     ((sin(psi)*sin(th)*sin(phi))+ ...
      (cos(psi)*cos(phi)))*V + ...
     ((sin(psi)*sin(th)*cos(phi))- ...
      (cos(psi)*sin(phi)))*W;

% z_d = (-1*sind(th)*U)+(cosd(th)*sind(phi)*V)+(cosd(th)*cosd(phi)*W);
z_d = (-1*sin(th)*U)+(cos(th)*sin(phi)*V)+(cos(th)*cos(phi)*W);

u_d = V*R - W*Q + gx + ((1/mass)*(fax+fpx));

v_d = W*P - U*R + gy + ((1/mass)*(fay+fpy));

w_d = U*Q - V*P + gz + ((1/mass)*(faz+fpz));

% phi_d = P +((sind(th)*sind(phi))/cosd(th))*Q + ((sind(th)*cosd(phi))/cosd(th))*R;
phi_d = P +((sin(th)*sin(phi))/cos(th))*Q + ((sin(th)*cos(phi))/cos(th))*R;

% th_d = cosd(phi)*Q - sind(phi)*R;
th_d = cos(phi)*Q - sin(phi)*R;

% psi_d = (sind(phi)/cosd(th))*Q + (cosd(phi)/cosd(th))*R;
psi_d = (sin(phi)/cos(th))*Q + (cos(phi)/cos(th))*R;

% intermediate calculations;
H_b_x = (iyy-izz)*Q*R + iyz*(Q^2-R^2) + (izx*Q - ixy*R)*P + la + lp;
H_b_y = (izz-ixx)*R*P + izx*(R^2-P^2) + (ixy*R - iyz*P)*Q + ma + mp;
H_b_z = (ixx-iyy)*P*Q + ixy*(P^2-Q^2) + (iyz*P - izx*Q)*R + na + np;

delta = (ixx*iyy*izz)-(ixx*iyz^2)-(iyy*izx^2)-(izz*ixy^2)-(2*ixy*iyz*izx);
ixx_inv = (iyy*izz-iyz^2)/delta;  ixy_inv = (izz*ixy+izx*iyz)/delta; % neg
iyy_inv = (ixx*izz-izx^2)/delta;  iyz_inv = (ixx*iyz+ixy*izx)/delta; % neg
izz_inv = (ixx*iyy-ixy^2)/delta;  izx_inv = (iyy*izx+ixy*iyz)/delta; % neg
% End intermediate calculations;

p_d = ixx_inv*H_b_x + ixy_inv*H_b_y + izx_inv*H_b_z;
q_d = ixy_inv*H_b_x + iyy_inv*H_b_y + iyz_inv*H_b_z;
r_d = izx_inv*H_b_x + iyz_inv*H_b_y + izz_inv*H_b_z;

x_vect = [x_d, y_d, z_d, u_d, v_d, w_d, phi_d, th_d, psi_d, p_d, q_d, r_d].';
end
%% Author: Joshua Reed, MAE 744
%
%  [cax caz cay cal cam can] = lookupforcoefficients(aero,input)
%
%   Inputs:  X     <- input vector containing aerodynamic conditions
%                    Vt, alfa, beta, P,Q,R
%
%            input <- Control surface inputs hrtz,ail,rdr,flap,spbr
%
%   Output:  cax,caz,cay,cal,cam,can
%
function [cax caz cay cal cam can] = calcoeff(X,input,Vt,alfa,beta)

% Load F-16 Data Tabels
declare_global;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TEST VALUES%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define Aerodynamic Parameters
% aero.Vt     = 550;              % Velocity (ft/s)
% aero.alfa   = 17;               % AoA Degrees
% aero.beta   = -21;              % Sideslip Degrees
% aero.Pa      = -37;              % Roll Rate  (Degree/s)
% aero.Qa      = 27;               % Pitch Rate (Degree/s)
% aero.Ra      = -17;              % Yaw Rat    (Degree/s)
% 
% % Define Control Input Parameters (Degrees)
% input.delH        = -17;      % Horizontal tail deflection (Pos down)
% input.delA        = 11;       % Aileron Deflection       (Right down Pos)
% input.delR        = 13;       % Rudder Defection          (Left is Pos)
% input.delFlap     = 15;       % Flap Defection            (Always Pos)
% input.delSpBr     = 9;        % Speedbrake Deflection     (Always Pos)
% 
% % Conversions
% aero.beta_rad = aero.beta*deg2rad;
% aero.P = aero.Pa*deg2rad;
% aero.Q = aero.Qa*deg2rad;
% aero.R = aero.Ra*deg2rad;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END TEST VALUES  %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute Aerodynamic Coefficients
%$$$$$$$$$$$$$$$$$$$$$$   Compute CAX $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
% Look up order: 1-D, 2-D, 3-D

% 1-D Look ups
cax_alfa_spbr     = lookup1d(alfa,alfa_aero_tab,cax_alfa_spbr_tab);
cax_alfa_q        = lookup1d(alfa,alfa_aero_tab,cax_alfa_q_tab);
cax_alfa_q_flaple = lookup1d(alfa,alfa_aero_flaple_tab, ...
    cax_alfa_q_flaple_tab);
% 2-D Look ups
cax_alfa_beta_flaple = lookup2d(alfa,beta, ...
    alfa_aero_flaple_tab,beta_aero_tab,cax_alfa_beta_flaple_tab);
% 3-D Look ups
cax_alfa_beta_hrzt  = lookup3d(alfa,beta, input(1), ...
    alfa_aero_tab,beta_aero_tab, hrzt_aero_long_tab ...
    ,cax_alfa_beta_hrzt_tab);

cax_alfa_beta_0hrzt  = lookup3d(alfa,beta, 0, ...
    alfa_aero_tab,beta_aero_tab, hrzt_aero_long_tab ...
    ,cax_alfa_beta_hrzt_tab);

% Intermediate caluclations
del_cax_alfa_beta_hrzt = cax_alfa_beta_flaple - cax_alfa_beta_0hrzt;

% Build CAX from components
cax = cax_alfa_beta_hrzt + ...
      (del_cax_alfa_beta_hrzt*(1-(input(4)/25))) + ...  
      (cax_alfa_spbr*(input(5)/60)) + ...
      ((cax_alfa_q + (cax_alfa_q_flaple*(1-(input(4)/25)))) ...
      *((cbar*X(11))/(2*Vt)));
  
 
%$$$$$$$$$$$$$$$$$$$$$$   Compute CAY $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

% 1-D Look ups
cay_alfa_p          = lookup1d(alfa,alfa_aero_tab,cay_alfa_p_tab);
cay_alfa_p_flaple   = lookup1d(alfa,alfa_aero_flaple_tab, ...
    cay_alfa_p_flaple_tab);
cay_alfa_r          = lookup1d(alfa,alfa_aero_tab,cay_alfa_r_tab);
cay_alfa_r_flaple   = lookup1d(alfa,alfa_aero_flaple_tab, ... 
    cay_alfa_r_flaple_tab);
% 2-D Look ups
cay_alfa_beta = lookup2d(alfa, beta, alfa_aero_tab, ...
    beta_aero_tab, cay_alfa_beta_tab);

cay_alfa_beta_flaple = lookup2d(alfa,beta, ...  
    alfa_aero_flaple_tab, beta_aero_tab, cay_alfa_beta_flaple_tab);

cay_alfa_beta_ail = lookup2d(alfa,beta, alfa_aero_tab, ...
    beta_aero_tab, cay_alfa_beta_ail_tab);

cay_alfa_beta_ail_flaple = lookup2d(alfa, beta, alfa_aero_flaple_tab, ...
    beta_aero_tab, cay_alfa_beta_ail_flaple_tab);

cay_alfa_beta_rdr = lookup2d(alfa,beta, alfa_aero_tab, ...
    beta_aero_tab, cay_alfa_beta_rdr_tab);

% Intermediate Calculations
del_cay_alfa_beta_flaple    = cay_alfa_beta_flaple - cay_alfa_beta;
del_cay_alfa_beta_rdr       = cay_alfa_beta_rdr - cay_alfa_beta;
del_cay_alfa_beta_ail       = cay_alfa_beta_ail - cay_alfa_beta;
del_cay_alfa_beta_ail_flaple = cay_alfa_beta_ail_flaple - ... 
    cay_alfa_beta_flaple - (cay_alfa_beta_ail - cay_alfa_beta);

% Build CAY from components
cay = cay_alfa_beta + del_cay_alfa_beta_flaple*(1-input(4)/25) ...
+ (del_cay_alfa_beta_ail + del_cay_alfa_beta_ail_flaple*(1-input(4)/25))*(input(2)/20) + del_cay_alfa_beta_rdr*(input(3)/30) ...
+ (cay_alfa_p + cay_alfa_p_flaple*(1-input(4)/25))*(bbar*X(10))/(2*Vt) + (cay_alfa_r + cay_alfa_r_flaple*(1-input(4)/25))*(bbar*X(12))/(2*Vt);

 %$$$$$$$$$$$$$$$$$$$$$$   Compute CAZ $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 % 1-D Look ups
caz_alfa_spbr     = lookup1d(alfa,alfa_aero_tab,caz_alfa_spbr_tab);
caz_alfa_q        = lookup1d(alfa,alfa_aero_tab,caz_alfa_q_tab);
caz_alfa_q_flaple = lookup1d(alfa,alfa_aero_flaple_tab, ...
    caz_alfa_q_flaple_tab);
% 2-D Look ups
caz_alfa_beta_flaple = lookup2d(alfa,beta, ...
    alfa_aero_flaple_tab,beta_aero_tab,caz_alfa_beta_flaple_tab);
% 3-D Look ups
caz_alfa_beta_hrzt  = lookup3d(alfa,beta, input(1), ...
    alfa_aero_tab,beta_aero_tab, hrzt_aero_long_tab ...
    ,caz_alfa_beta_hrzt_tab);

caz_alfa_beta_hrzt0  = lookup3d(alfa,beta, 0, ...
    alfa_aero_tab,beta_aero_tab, hrzt_aero_long_tab ...
    ,caz_alfa_beta_hrzt_tab);

% Intermediate caluclations
del_caz_alfa_beta_flaple = caz_alfa_beta_flaple - caz_alfa_beta_hrzt0;

% Build CAZ from components  (Double Check)
caz = caz_alfa_beta_hrzt + ...
      (del_caz_alfa_beta_flaple*(1-(input(4)/25))) + ...  
      (caz_alfa_spbr*(input(5)/60)) + ...
      ((caz_alfa_q + (caz_alfa_q_flaple*(1-(input(4)/25)))) ...
      *((cbar*X(11))/(2*Vt)));
 
 %$$$$$$$$$$$$$$$$$$$$$$   Compute CAL $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 % 1-D Look Ups
 cal_alfa_p         = lookup1d(alfa,alfa_aero_tab,cal_alfa_p_tab);
 cal_alfa_p_flaple  = lookup1d(alfa,alfa_aero_flaple_tab, ...
     cal_alfa_p_flaple_tab);
 cal_alfa_r         = lookup1d(alfa,alfa_aero_tab,cal_alfa_r_tab);
 cal_alfa_r_flaple  = lookup1d(alfa,alfa_aero_flaple_tab, ...
     cal_alfa_r_flaple_tab);
 cal_alfa_beta      = lookup1d(alfa,alfa_aero_tab, ...
     cal_alfa_beta_tab);
 % 2-D Look Ups
 cal_alfa_beta_flaple  = lookup2d(alfa,beta, ...
     alfa_aero_flaple_tab,beta_aero_tab,cal_alfa_beta_flaple_tab);
 cal_alfa_beta_ail     = lookup2d(alfa,beta, ...
     alfa_aero_tab,beta_aero_tab,cal_alfa_beta_ail_tab);
 cal_alfa_beta_ail_flaple = lookup2d(alfa,beta, ...
     alfa_aero_flaple_tab,beta_aero_tab,cal_alfa_beta_ail_flaple_tab);
 cal_alfa_beta_rdr     = lookup2d(alfa,beta, ...
     alfa_aero_tab,beta_aero_tab,cal_alfa_beta_rdr_tab);
 % 3-D Look Ups
 cal_alfa_beta_hrzt = lookup3d(alfa,beta,input(1), ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_latdir_tab,cal_alfa_beta_hrzt_tab); 
 cal_alfa_beta_hrzt0 = lookup3d(alfa,beta,0, ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_latdir_tab,cal_alfa_beta_hrzt_tab); 
 
 % Intermediate Calculations
 del_cal_alfa_beta_flaple = cal_alfa_beta_flaple - cal_alfa_beta_hrzt0;
 del_cal_alfa_beta_rdr = cal_alfa_beta_rdr - cal_alfa_beta_hrzt0;
 del_cal_alfa_beta_ail = cal_alfa_beta_ail - cal_alfa_beta_hrzt0;
 del_cal_alfa_beta_ail_flaple = cal_alfa_beta_ail_flaple - ...
     cal_alfa_beta_flaple - (cal_alfa_beta_ail - cal_alfa_beta_hrzt0);
 
 % Build CAL from components 
cal = cal_alfa_beta_hrzt + del_cal_alfa_beta_flaple*(1-input(4)/25) + cal_alfa_beta*deg2rad(beta) ...
+ (del_cal_alfa_beta_ail + del_cal_alfa_beta_ail_flaple*(1-input(4)/25))*(input(2)/20) + del_cal_alfa_beta_rdr*(input(3)/30) ...
+ (cal_alfa_p + cal_alfa_p_flaple*(1-input(4)/25))*(bbar*X(10))/(2*Vt) + (cal_alfa_r + cal_alfa_r_flaple*(1-input(4)/25))*(bbar*X(12))/(2*Vt);
 
 %$$$$$$$$$$$$$$$$$$$$$$   Compute CAM $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 % 1-D Look Ups
 cam_alfa =  lookup1d(alfa,alfa_aero_tab,cam_alfa_tab);
 mu_camabh = lookup1d(input(1),hrzt_aero_long_tab,mu_camabh_tab);
 cam_alfa_q_flaple = lookup1d(alfa,alfa_aero_flaple_tab, ...
     cam_alfa_q_flaple_tab);
 cam_alfa_q = lookup1d(alfa,alfa_aero_tab,cam_alfa_q_tab);
 cam_alfa_spbr = lookup1d(alfa,alfa_aero_tab,cam_alfa_spbr_tab);
 % 2-D Look Ups
 cam_alfa_hrzt = lookup2d(alfa,input(1),alfa_aero_tab, ...
     hrzt_aero_long_cama_tab,cam_alfa_hrzt_tab);
 cam_alfa_beta_flaple = lookup2d(alfa,beta, ...
     alfa_aero_flaple_tab,beta_aero_tab,cam_alfa_beta_flaple_tab);
 % 3-D Look Ups
 cam_alfa_beta_hrzt = lookup3d(alfa,beta,input(1), ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_long_tab , ...
     cam_alfa_beta_hrzt_tab);
 cam_alfa_beta_hrzt0 = lookup3d(alfa,beta,0, ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_long_tab , ...
     cam_alfa_beta_hrzt_tab);
 
 % Intermediate Calculations
 del_cam_alfa_beta_flaple = cam_alfa_beta_flaple - cam_alfa_beta_hrzt0;
 
 % Check if xcm has changed. If so, recalculate caz.
 
 % Build CAM out of components
 cam = (cam_alfa_beta_hrzt*mu_camabh) + ...
     (del_cam_alfa_beta_flaple*(1-(input(4)/25))) + ...
     (cam_alfa_spbr*(input(5)/60)) + ...
     ((cam_alfa_q + cam_alfa_q_flaple*(1-(input(4)/25)))* ...
     ((cbar*X(11))/(2*Vt))) + ...
     cam_alfa + ...
     cam_alfa_hrzt + ...
     (caz*(xcmbr-xcmb));
     
 
 %$$$$$$$$$$$$$$$$$$$$$$   Compute CAN $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 % 1-D Look Ups
 %can_alfa_ail  = lookup1d(alfa,alfa_aero_tab,can_alfa_ail_tab);
 can_alfa_beta = lookup1d(alfa,alfa_aero_tab,can_alfa_beta_tab);
 can_alfa_r_flaple = lookup1d(alfa,alfa_aero_flaple_tab, ...
     can_alfa_r_flaple_tab);
 can_alfa_r = lookup1d(alfa,alfa_aero_tab,can_alfa_r_tab);
 can_alfa_p_flaple = lookup1d(alfa,alfa_aero_flaple_tab, ...
     can_alfa_p_flaple_tab);
 can_alfa_p = lookup1d(alfa,alfa_aero_tab,can_alfa_p_tab);
% 2-D Look Ups
 can_alfa_beta_rdr    = lookup2d(alfa,beta, ...
     alfa_aero_tab,beta_aero_tab,can_alfa_beta_rdr_tab);
 can_alfa_beta_ail_flaple = lookup2d(alfa,beta, ...
     alfa_aero_flaple_tab,beta_aero_tab,can_alfa_beta_ail_flaple_tab);
 can_alfa_beta_ail    = lookup2d(alfa,beta, ...
     alfa_aero_tab,beta_aero_tab,can_alfa_beta_ail_tab); 
 can_alfa_beta_flaple = lookup2d(alfa,beta, ...
     alfa_aero_flaple_tab,beta_aero_tab,can_alfa_beta_flaple_tab);
% 3-D Look Ups
 can_alfa_beta_hrzt = lookup3d(alfa,beta,input(1), ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_latdir_tab, ...
     can_alfa_beta_hrzt_tab);
 can_alfa_beta_hrzt0 = lookup3d(alfa,beta,0, ...
     alfa_aero_tab,beta_aero_tab,hrzt_aero_latdir_tab, ...
     can_alfa_beta_hrzt_tab);

% Intermediate Calculations
del_can_alfa_beta_flaple = can_alfa_beta_flaple - can_alfa_beta_hrzt0;
del_can_alfa_beta_rdr = can_alfa_beta_rdr - can_alfa_beta_hrzt0;
del_can_alfa_beta_ail = can_alfa_beta_ail -can_alfa_beta_hrzt0;
del_can_alfa_beta_ail_flaple = can_alfa_beta_ail_flaple ... 
    - can_alfa_beta_flaple - (can_alfa_beta_ail - can_alfa_beta_hrzt0);

% Build CAN out of components
can = can_alfa_beta_hrzt + del_can_alfa_beta_flaple*(1-input(4)/25) + can_alfa_beta*deg2rad(beta) ...
+ (del_can_alfa_beta_ail + del_can_alfa_beta_ail_flaple*(1-input(4)/25))*(input(2)/20) + del_can_alfa_beta_rdr*(input(3)/30) ...
+ (can_alfa_p + can_alfa_p_flaple*(1-input(4)/25))*(bbar*X(10))/(2*Vt) + (can_alfa_r + can_alfa_r_flaple*(1-input(4)/25))*(bbar*X(12))/(2*Vt) ...
- cay*(xcmbr-xcmb)*cbar/bbar;


% Output
% fprintf(1,'cax: %f\n',cax); 
% fprintf(1,'cay:  %f\n',cay);
% fprintf(1,'caz:  %f\n',caz);
% fprintf(1,'cal:  %f\n',cal);
% fprintf(1,'cam:  %f\n',cam);
% fprintf(1,'can:  %f\n',can);

end
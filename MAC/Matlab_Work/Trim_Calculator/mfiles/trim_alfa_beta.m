function f_trim = trim_alfa_beta(guess)
    %% Load Data Dependencies
    declare_global;

    %%Build X,U
%   X = [aero.X aero.Y aero.Z aero.Vt aero.alfa aero.beta_rad aero.phi ...
%       aero.th aero.psi aero.P aero.Q aero.R].';
    X = [aero.X aero.Y aero.Z aero.Vt guess(2) guess(3) aero.phi guess(1) ...
        aero.psi aero.P aero.Q aero.R].';
%     U = [ctrlin.delH ctrlin.delA ctrlin.delR ctrlin.delFlap ...
%         ctrlin.delSpBr ctrlin.thtl].';
    U = [guess(4) guess(5) guess(6) ctrlin.delFlap ctrlin.delSpBr ...
         guess(7)].';
     
    X_dot = EOM(X,U);

    alfa = guess(2); 
    beta = guess(3);
    theta= guess(1);
    phi = X(7);

    f_trim = [
        X_dot(4);
        X_dot(5);
        X_dot(6);
        X_dot(10);
        X_dot(11);
        X_dot(12);
       (-1*cosd(alfa)*cosd(beta)*sind(theta)) + ...
       (sind(phi)*sind(beta)+cosd(phi)*sind(alfa)*cosd(beta))*cosd(theta)
       ];

end




%     gx = -sind(theta)*g;
%     gy =  cosd(theta)*sind(phi)*g;
%     gz =  cosd(theta)*cosd(phi)*g;
%     
% %     [cax caz cay cal cam can] = calcoeff(X,U);
%     [cax caz cay cal cam can] = calc_aero_coeff(X,U);
%     
%     rho    = lookup1d(H,alt_e_tab,rho_e_tab);
%     Vs     = lookup1d(H,alt_e_tab,vs_e_tab);
%     qbar = 0.5*rho*(X(4)^2);
% 
%     fax = qbar * sbar * cax;
%     fay = qbar * sbar * cay;
%     faz = qbar * sbar * caz;
%     la  = qbar * sbar * bbar * cal;
%     ma  = qbar * sbar * cbar * cam;
%     na  = qbar * sbar * bbar * can;
%     
%     fpx = calc_prop_coeff(X,U,Vs); 
%     fpy = 0;
%     fpz = 0;
%     lp  = 0;
%     mp  = 0;
%     np  = 0;
%         f_trim = [
%             gx + (fax + fpx)/mass;
%             gy + (fay + fpy)/mass;
%             gz + (faz + fpz)/mass;
%             la + lp;
%             ma + mp;
%             na + np;
%             (-1*cosd(alfa)*cosd(beta)*sind(theta)) + ...
%             (sind(phi)*sind(beta)+cosd(phi)*sind(alfa)*cosd(beta))*cosd(theta)
%             ];
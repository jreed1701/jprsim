function [fax fay faz fpx fpy fpz gx gy gz la ma na lp mp np P Q R psi th phi U V W] = reassign(X,acdata)
% Just reassigning inputs to make coding easier.
%% acdata
gx  = acdata(1);
gy  = acdata(2);
gz  = acdata(3);
U   = acdata(4);
V   = acdata(5);
W   = acdata(6);
fax = acdata(7);
fay = acdata(8);
faz = acdata(9);
la  = acdata(10);
ma  = acdata(11);
na  = acdata(12);
fpx = acdata(13);
fpy = acdata(14);
fpz = acdata(15);
lp  = acdata(16);
mp  = acdata(17);
np  = acdata(18);

% aero values
P   = X(10);
Q   = X(11);
R   = X(12);
phi = X(7);
th  = X(8);
psi = X(9);


end
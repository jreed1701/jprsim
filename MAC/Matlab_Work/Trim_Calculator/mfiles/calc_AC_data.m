function acdata = calc_AC_data(X,input,altvars)

declare_global;

% Calculate gravity scalars
% gx = -sind(X(8))*g;
% gy =  cosd(X(8))*sind(X(7))*g;
% gz =  cosd(X(8))*cosd(X(7))*g;

gx = -sin(X(8))*g;
gy =  cos(X(8))*sin(X(7))*g;
gz =  cos(X(8))*cos(X(7))*g;

% Calculate velocity scalars & Velocity here should be Ground Speed
% That means you'll have to recalc Vt for wind effects.
% U = aero.Vt*cosd(aero.alfa)*cosd(aero.beta);
% V = aero.Vt*sind(aero.beta);
% W = aero.Vt*sind(aero.alfa)*cosd(aero.beta);
U = X(4);
V = X(5);
W = X(6);
% Calculate Vt, alfa, beta
Vt = sqrt(U^2 + V^2 + W^2);
alfa = atand(W/U);
beta = atand(V/sqrt(U^2 + W^2));
% Calculate aerodynamic coefficients
[cax caz cay cal cam can] = calcoeff(X,input,Vt,alfa,beta);
% [cax caz cay cal cam can] = calc_aero_coeff(X,input);

qbar = 0.5*altvars(3)*(Vt^2); % Should be True Air Speed

% Calculate aerodynamic forces
fax = qbar * sbar * cax;
fay = qbar * sbar * cay;
faz = qbar * sbar * caz;
la  = qbar * sbar * bbar * cal;
ma  = qbar * sbar * cbar * cam;
na  = qbar * sbar * bbar * can;

% Calculate propulsive forces
fpx = calc_prop_coeff(X,input,Vt,altvars(4)); 
fpy = 0;
fpz = 0;
lp  = 0;
mp  = 0;
np  = 0;
% Thrust directly down nose of A/C
% Can expand later for thrust vectoring.
% fpy,fpz,lp,mp,np remain zero

% acdata = [gx gy gz U V W fax fay faz la ma na fpx fpy fpz lp mp np].';
acdata = [gx gy gz U V W fax fay faz la ma na fpx fpy fpz lp mp np].';

end
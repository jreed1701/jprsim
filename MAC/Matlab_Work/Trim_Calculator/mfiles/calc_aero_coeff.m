function [cax caz cay cal cam can] = calc_aero_coeff(X,U)

declare_global;
% aero_dat_f16b
% geom_dat_f16b
% iner_dat_f16b
% prop_dat_f16b
% atmos_dat

pi = 3.14159265358979;
d2r = pi/180;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% given %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Vt     = X(4);     %ft/s
alpha  = X(5);      %deg
beta   = X(6);     %deg
P      = X(10);     %rad/s
Q      = X(11);     %rad/s
R      = X(12);     %rad/s
delH   = U(1);     %deg
delA   = U(2);      %deg
delR   = U(3);      %deg
delFLE = U(4);      %deg
delSB  = U(5);       %deg
% P = P * d2r;
% Q = Q * d2r;
% R = R * d2r;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% cax %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cax_alfa_beta_hrzt       = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  cax_alfa_beta_hrzt_tab,      beta,alpha,delH);
cax_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cax_alfa_beta_flaple_tab,    beta,alpha);
cax_alfa_beta_hrzt_0     = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  cax_alfa_beta_hrzt_tab,      beta,alpha,0);
cax_alfa_spbr            = interp1(alfa_aero_tab,                                   cax_alfa_spbr_tab,           alpha);
cax_alfa_q               = interp1(alfa_aero_tab,                                   cax_alfa_q_tab,              alpha);
cax_alfa_q_flaple        = interp1(alfa_aero_flaple_tab,                            cax_alfa_q_flaple_tab,       alpha);

del_cax_flaple           = cax_alfa_beta_flaple     - cax_alfa_beta_hrzt_0;

cax = cax_alfa_beta_hrzt + del_cax_flaple*(1 - delFLE/25) + cax_alfa_spbr*delSB/60 + ...
    (cax_alfa_q + cax_alfa_q_flaple*(1 - delFLE/25))*cbar*Q/2/Vt;
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% cay %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cay_alfa_beta            = interp2(beta_aero_tab,alfa_aero_tab,                     cay_alfa_beta_tab,           beta,alpha);
cay_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cay_alfa_beta_flaple_tab,    beta,alpha);
cay_alfa_beta_rdr        = interp2(beta_aero_tab,alfa_aero_tab,                     cay_alfa_beta_rdr_tab,       beta,alpha);
cay_alfa_beta_ail        = interp2(beta_aero_tab,alfa_aero_tab,                     cay_alfa_beta_ail_tab,       beta,alpha);
cay_alfa_beta_ail_flaple = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cay_alfa_beta_ail_flaple_tab,beta,alpha);
cay_alfa_p               = interp1(alfa_aero_tab,                                   cay_alfa_p_tab,              alpha);
cay_alfa_p_flaple        = interp1(alfa_aero_flaple_tab,                            cay_alfa_p_flaple_tab,       alpha);
cay_alfa_r               = interp1(alfa_aero_tab,                                   cay_alfa_r_tab,              alpha);
cay_alfa_r_flaple        = interp1(alfa_aero_flaple_tab,                            cay_alfa_r_flaple_tab,       alpha);

del_cay_flaple           = cay_alfa_beta_flaple     - cay_alfa_beta;
del_cay_rdr              = cay_alfa_beta_rdr        - cay_alfa_beta;
del_cay_ail              = cay_alfa_beta_ail        - cay_alfa_beta;
del_cay_ail_flaple       = cay_alfa_beta_ail_flaple - cay_alfa_beta_flaple - del_cay_ail;

cay = cay_alfa_beta + del_cay_flaple*(1 - delFLE/25) + del_cay_rdr*delR/30 + ...
    (del_cay_ail + del_cay_ail_flaple*(1 - delFLE/25))*delA/20 + ...
    (cay_alfa_p + cay_alfa_p_flaple*(1 - delFLE/25))*bbar*P/2/Vt + ...
    (cay_alfa_r + cay_alfa_r_flaple*(1 - delFLE/25))*bbar*R/2/Vt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% caz %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
caz_alfa_beta_hrzt       = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  caz_alfa_beta_hrzt_tab,      beta,alpha,delH);
caz_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              caz_alfa_beta_flaple_tab,    beta,alpha);
caz_alfa_beta_hrzt_0     = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  caz_alfa_beta_hrzt_tab,      beta,alpha,0);
caz_alfa_spbr            = interp1(alfa_aero_tab,                                   caz_alfa_spbr_tab,           alpha);
caz_alfa_q               = interp1(alfa_aero_tab,                                   caz_alfa_q_tab,              alpha);
caz_alfa_q_flaple        = interp1(alfa_aero_flaple_tab,                            caz_alfa_q_flaple_tab,       alpha);

del_caz_flaple           = caz_alfa_beta_flaple     - caz_alfa_beta_hrzt_0;

caz = caz_alfa_beta_hrzt + del_caz_flaple*(1 - delFLE/25) + caz_alfa_spbr*delSB/60 + ...
    (caz_alfa_q + caz_alfa_q_flaple*(1 - delFLE/25))*cbar*Q/2/Vt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% cal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cal_alfa_beta_hrzt       = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_latdir_tab,cal_alfa_beta_hrzt_tab,      beta,alpha,delH);
cal_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cal_alfa_beta_flaple_tab,    beta,alpha);
cal_alfa_beta_hrzt_0     = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_latdir_tab,cal_alfa_beta_hrzt_tab,      beta,alpha,0);
cal_alfa_beta            = interp1(alfa_aero_tab,                                   cal_alfa_beta_tab,           alpha);
cal_alfa_beta_rdr        = interp2(beta_aero_tab,alfa_aero_tab,                     cal_alfa_beta_rdr_tab,beta,  alpha);
cal_alfa_beta_ail        = interp2(beta_aero_tab,alfa_aero_tab,                     cal_alfa_beta_ail_tab,beta,  alpha);
cal_alfa_beta_ail_flaple = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cal_alfa_beta_ail_flaple_tab,beta,alpha);
cal_alfa_p               = interp1(alfa_aero_tab,                                   cal_alfa_p_tab,              alpha);
cal_alfa_p_flaple        = interp1(alfa_aero_flaple_tab,                            cal_alfa_p_flaple_tab,       alpha);
cal_alfa_r               = interp1(alfa_aero_tab,                                   cal_alfa_r_tab,alpha);
cal_alfa_r_flaple        = interp1(alfa_aero_flaple_tab,                            cal_alfa_r_flaple_tab,       alpha);

del_cal_flaple           = cal_alfa_beta_flaple     - cal_alfa_beta_hrzt_0;
del_cal_rdr              = cal_alfa_beta_rdr        - cal_alfa_beta_hrzt_0;
del_cal_ail              = cal_alfa_beta_ail        - cal_alfa_beta_hrzt_0;
del_cal_ail_flaple       = cal_alfa_beta_ail_flaple - cal_alfa_beta_flaple - del_cal_ail;

cal = cal_alfa_beta_hrzt + del_cal_flaple*(1 - delFLE/25) + cal_alfa_beta*beta*d2r + ...
    del_cal_rdr*delR/30 + (del_cal_ail + del_cal_ail_flaple*(1 - delFLE/25))*delA/20 + ...
    (cal_alfa_p + cal_alfa_p_flaple*(1 - delFLE/25))*bbar*P/2/Vt + ...
    (cal_alfa_r + cal_alfa_r_flaple*(1 - delFLE/25))*bbar*R/2/Vt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% cam %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cam_alfa_beta_hrzt       = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  cam_alfa_beta_hrzt_tab,      beta,alpha,delH);
mu_camabh                = interp1(hrzt_aero_long_tab,                              mu_camabh_tab,               delH);
cam_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              cam_alfa_beta_flaple_tab,    beta,alpha);
cam_alfa_beta_hrzt_0     = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_long_tab,  cam_alfa_beta_hrzt_tab,      beta,alpha,0);
cam_alfa_spbr            = interp1(alfa_aero_tab,                                   cam_alfa_spbr_tab,           alpha);
cam_alfa_q               = interp1(alfa_aero_tab,                                   cam_alfa_q_tab,              alpha);
cam_alfa_q_flaple        = interp1(alfa_aero_flaple_tab,                            cam_alfa_q_flaple_tab,       alpha);
cam_alfa                 = interp1(alfa_aero_tab,                                   cam_alfa_tab,                alpha);
cam_alfa_hrzt            = interp2(hrzt_aero_long_cama_tab,alfa_aero_tab,           cam_alfa_hrzt_tab,           delH,alpha);

del_cam_flaple           = cam_alfa_beta_flaple     - cam_alfa_beta_hrzt_0;

cam = cam_alfa_beta_hrzt*mu_camabh + del_cam_flaple*(1 - delFLE/25) + ...
    cam_alfa_spbr*delSB/60 + (cam_alfa_q + cam_alfa_q_flaple*(1 - delFLE/25))*cbar*Q/2/Vt + ...
    cam_alfa + cam_alfa_hrzt + caz*(xcmbr - xcmb);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% can %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
can_alfa_beta_hrzt       = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_latdir_tab,can_alfa_beta_hrzt_tab,      beta,alpha,delH);
can_alfa_beta_flaple     = interp2(beta_aero_tab,alfa_aero_flaple_tab,              can_alfa_beta_flaple_tab,    beta,alpha);
can_alfa_beta_hrzt_0     = interp3(beta_aero_tab,alfa_aero_tab,hrzt_aero_latdir_tab,can_alfa_beta_hrzt_tab,      beta,alpha,0);
can_alfa_beta            = interp1(alfa_aero_tab,                                   can_alfa_beta_tab,           alpha);
can_alfa_beta_rdr        = interp2(beta_aero_tab,alfa_aero_tab,                     can_alfa_beta_rdr_tab,       beta,alpha);
can_alfa_beta_ail        = interp2(beta_aero_tab,alfa_aero_tab,                     can_alfa_beta_ail_tab,       beta,alpha);
can_alfa_beta_ail_flaple = interp2(beta_aero_tab,alfa_aero_flaple_tab,              can_alfa_beta_ail_flaple_tab,beta,alpha);
can_alfa_p               = interp1(alfa_aero_tab,                                   can_alfa_p_tab,              alpha);
can_alfa_p_flaple        = interp1(alfa_aero_flaple_tab,                            can_alfa_p_flaple_tab,       alpha);
can_alfa_r               = interp1(alfa_aero_tab,                                   can_alfa_r_tab,              alpha);
can_alfa_r_flaple        = interp1(alfa_aero_flaple_tab,                            can_alfa_r_flaple_tab,       alpha);

del_can_flaple           = can_alfa_beta_flaple     - can_alfa_beta_hrzt_0;
del_can_rdr              = can_alfa_beta_rdr        - can_alfa_beta_hrzt_0;
del_can_ail              = can_alfa_beta_ail        - can_alfa_beta_hrzt_0;
del_can_ail_flaple       = can_alfa_beta_ail_flaple - can_alfa_beta_flaple - del_can_ail;

can = can_alfa_beta_hrzt + del_can_flaple*(1 - delFLE/25) + can_alfa_beta*beta + ...
    del_can_rdr*delR/30 + (del_can_ail + del_can_ail_flaple*(1 - delFLE/25))*delA/20 + ...
    (can_alfa_p + can_alfa_p_flaple*(1 - delFLE/25))*bbar*P/2/Vt + ...
    (can_alfa_r + can_alfa_r_flaple*(1 - delFLE/25))*bbar*R/2/Vt - cay*(xcmbr - xcmb)*cbar/bbar;


% fprintf('cax: %20.10e \n',cax)
% fprintf('cay: %20.10e \n',cay)
% fprintf('caz: %20.10e \n',caz)
% fprintf('cal: %20.10e \n',cal)
% fprintf('cam: %20.10e \n',cam)
% fprintf('can: %20.10e \n',can)
end
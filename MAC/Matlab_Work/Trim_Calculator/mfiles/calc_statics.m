function f_trim = calc_statics()

declare_global;
% Guesses
alfa = 0;
beta = 0;
theta = 30;
delH = -20;
delA = 0;
delR = 0;
Thtl = 0.5;

% Build vector from guesses
% guess = [theta, alfa, beta, delH, delA, delR, Thtl].';
guess = [alfa, delH, Thtl].';

% Configure optimset for fsolve
settings = optimset(         'fsolve'             );
settings = optimset(settings,'Display',     'iter');
settings = optimset(settings,'TolX'         ,1e-12);
settings = optimset(settings,'TolFun'       ,1e-12);
settings = optimset(settings,'Tolcon'       ,1e-12);
settings = optimset(settings,'MaxIter'      ,5000);
settings = optimset(settings,'MaxFunEvals'  ,5000);

% Run Fsolve

f_trim = fsolve('trim_eq',guess,settings);
% f_trim = fsolve('trim_alfa_beta',guess,settings);
% f_trim = fsolve('Advanced_trim_equation',guess,settings);

% check_hard_limits(f_trim);

end
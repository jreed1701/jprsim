function propdata = calc_prop_coeff(X,U,Vt,Vs) 

declare_global;

H = -1*X(3);
mach = Vt/Vs; % Vt/Vs

% fprintf(1,'Throttle:  %f -\n', U(6));

if  U(6) < thtl_ab_on_off %U(6) >= 0 &&
    power = k_thtl_ab_off_1*U(6) + k_thtl_ab_off_0;
elseif U(6) >= thtl_ab_on_off %&& U(6) <= 1
    power = k_thtl_ab_on_1*U(6) + k_thtl_ab_on_0;
end
% elseif(input(6)>1)
%     disp('You can not have a throttle setting greater than 1.0');
% end

propdata = lookup3d(H,mach,power,alt_prop_tab,mach_prop_tab,powr_prop_tab,...
    thst_alt_mach_powr_tab);

end
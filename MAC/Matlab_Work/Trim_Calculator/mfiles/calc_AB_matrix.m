function [alfa beta] = calc_AB_matrix(aero)

declare_global;

% Guesses
alfa = 4;
beta = 4;
theta = 3;
delH = -1.5;
delA = -2;
delR = 15;
Thtl = 0.25;

% Build vector from guesses
guess = [theta, alfa, beta, delH, delA, delR, Thtl].';

% Configure optimset for fsolve
settings = optimset(         'fsolve'             );
settings = optimset(settings,'Display',     'iter');
settings = optimset(settings,'TolX'         ,1e-12);
settings = optimset(settings,'TolFun'       ,1e-12);
settings = optimset(settings,'MaxIter'      ,100);
settings = optimset(settings,'MaxFunEvals'  ,100);

% Run Fsolve
aero.Vt = 112; % ft/s
aero.phi= 0;   % deg
alfa = zeros();
beta = zeros();
    for i = 1:1:9144; % up to mach .9
        for j = 1:1:3600 
            [f_trim fval exitflag] = fsolve('trim_alfa_beta',guess,settings);
            exit = check_hard_limits(f_trim);
            % if hard limit met, save alfa beta pair
            if(exit(1) == 1 || exit(1) == 1 || exit(1) == 1 || exit(1) == 1)
                alfa(i,j) = f_trim(2);
                beta(i,j) = f_trim(3);
            end
            % if no hard limit met, but fsolve couldn't find a soln,
            % save pair.
%             if ( exitflag < 0 )
%                 alfa(i,j) = f_trim(2);
%                 beta(i,j) = f_trim(3);
%             end
            aero.phi = aero.phi + .1;
        end
        aero.Vt = aero.Vt + .1;
    end
end
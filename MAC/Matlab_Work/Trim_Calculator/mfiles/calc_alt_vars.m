function altvars = calc_alt_vars(H)

declare_global;
Temp   = lookup1d(H,alt_e_tab,temp_e_tab);
Pres   = lookup1d(H,alt_e_tab,pres_e_tab);
rho    = lookup1d(H,alt_e_tab,rho_e_tab);
Vs     = lookup1d(H,alt_e_tab,vs_e_tab);

altvars = [Temp Pres rho Vs].';

end
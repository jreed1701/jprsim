function exit = check_hard_limits(f_trim)

fprintf(1,'\n');
fprintf(1,'$$$$$$$$$$$$$$$$$$$$$$$$$$$\n');
fprintf(1,'\n');

if(abs(f_trim(4)) > 25) % Elevator limit
    hlim = 1;
    disp('Elevator has exceeded limits');
else
    hlim = 0;
end

if(abs(f_trim(5)) > 21.5) % Aeileron limit
    alim = 1;
    disp('Aeileron has exceeded limits');
else
    alim = 0;
end

if(abs(f_trim(6)) > 30) % Rudder limit
    rlim = 1;
    disp('Rudder has exceeded limits');
else
    rlim = 0;
end

if(abs(f_trim(7)) >= .99) % throttle limit
    tlim = 1;
    disp('Throttle is at or beyond 99%');
else
    tlim = 0;
end

fprintf(1,'\n');
fprintf(1,'$$$$$$$$$$$$$$$$$$$$$$$$$$$\n');
fprintf(1,'\n');

exit = [ hlim alim rlim tlim].';

end
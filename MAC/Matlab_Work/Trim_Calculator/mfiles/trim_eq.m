function f_trim = trim_eq(guess_vect)
    %% Load Data Dependencies
    declare_global;
    
    trimvt = 250;

    %%Build X,U
    X = [aero.X aero.Y aero.Z aero.Vt guess_vect(1) aero.beta aero.phi aero.th ...
        aero.psi aero.P aero.Q aero.R].';
%     X = [5 -3 -25000 600 guess_vect(1) 0 0 0 0 0 0 0].';

    U = [guess_vect(2) ctrlin.delA ctrlin.delR ctrlin.delFlap ctrlin.delSpBr ...
         guess_vect(3)].';
%     U = [guess_vect(2) 0 0 0 0 guess_vect(3)].';
    
    H = -X(3);
    rho    = lookup1d(H,alt_e_tab,rho_e_tab);
    Vs     = lookup1d(H,alt_e_tab,vs_e_tab);
    qbar = 0.5*rho*(trimvt^2);
    
    [cax caz cay cal cam can] = calcoeff(X,U,trimvt,guess_vect(1),aero.beta);
    
    % Calculate aerodynamic forces
    fax = qbar * sbar * cax;
    fay = qbar * sbar * cay;
    faz = qbar * sbar * caz;
    la  = qbar * sbar * bbar * cal;
    ma  = qbar * sbar * cbar * cam;
    na  = qbar * sbar * bbar * can;

    % Calculate propulsive forces
    fpx = calc_prop_coeff(X,U,trimvt,Vs); 
    fpy = 0;
    fpz = 0;
    lp  = 0;
    mp  = 0;
    np  = 0;
    
    % For this problem Theta = Alfa
    gx = -sind(guess_vect(1))*g;
    gy =  cosd(guess_vect(1))*sin(X(7))*g;
    gz =  cosd(guess_vect(1))*cos(X(7))*g;
    
    f_trim = [(gx + (fax + fpx)/mass);
              (gz + (faz + fpz)/mass);
              (ma + mp)].';
end

%% Author: Joshua Reed, MAE 744
% Function main(sec)
% inputs:  seconds  to run.

function main(sec,step)
    close all; %format long e; format compact;
    %%% Add mfiles directory to path (Lets me keep my source files
    %%% organized)
    addPath = sprintf('%s%s',pwd,'/Data');
    path(addPath,path);
    addPath = sprintf('%s%s',pwd,'/mfiles');
    path(addPath,path);
    addPath = sprintf('%s%s',pwd,'/initialization');
    path(addPath,path);
    %% Load Data Dependencies
    declare_global;
    loaddata;
    %% Initialize for trim calc;
    [aero ctrlin] = initialize2; %#ok<ASGLU,NASGU>
    % Get Trim values
    f_trim = calc_statics();
    % Initialize Data Structures with trim values.
    [aero ctrlin] = initialize4(f_trim);
    %% Define Initial State space vectors & Constants.
    X0 = [aero.X aero.Y aero.Z aero.U aero.V aero.W aero.phi ...
        aero.th aero.psi aero.P aero.Q aero.R].';

    U0 = [ctrlin.delH ctrlin.delA ctrlin.delR ctrlin.delFlap ...
        ctrlin.delSpBr ctrlin.thtl].';
    %% Set up time vector
    dt = step;
    t=0:dt:sec;
    %% Begin Master Loop
    x(:,1) = X0;
    START = tic;
    for i=2:length(t);
        
        if i <= 2
            u = U0;
        else     
%             Elevator Step
            u = [(ctrlin.delH-1) ctrlin.delA ctrlin.delR ctrlin.delFlap...
            ctrlin.delSpBr ctrlin.thtl].';

%         elseif (i > 2 && i <= (1/step))
% %           Aileron Step
%             u = [ctrlin.delH (ctrlin.delA-1) ctrlin.delR ctrlin.delFlap...
%             ctrlin.delSpBr ctrlin.thtl].';
%         else
            u = U0;
        end

        x1 = x(:,i-1) + dt/2* EOM(x(:,i-1),u);
        x2 = x(:,i-1) + dt/2* EOM(x1,u);
        x3 = x(:,i-1) + dt*   EOM(x2,u);
        x4 = x(:,i-1) + dt*   EOM(x3,u);
    
        x(:,i) = -.5*x(:,i-1) + 1/6*(2*x1 + 4*x2 + 2*x3 + x4);
        
        [p(1) p(2) p(3)] = ecef2lla(x(1,i),x(2,i),x(3,i),37.6272,-122.3896);
        p(4)   = rad2deg(x(7,i));
        p(5)   = rad2deg(x(8,i));
        p(6)   = rad2deg(x(9,i));

%         for j = 1:6 
%            fprintf(net, '%f\n',p(j));
%         end
    end
    
    figure(1);
    subplot(4,3,1);  plot(t,x(1,:));  title('X'); 
    xlabel('Seconds'); ylabel('ft');
    subplot(4,3,2);  plot(t,x(2,:));  title('Y');
    xlabel('Seconds'); ylabel('ft');
    subplot(4,3,3);  plot(t,-x(3,:));  title('-Z');
    xlabel('Seconds'); ylabel('Altitude');

    subplot(4,3,4);  plot(t,x(4,:));  title('U');
    xlabel('Seconds'); ylabel('ft/s');
    subplot(4,3,5);  plot(t,x(5,:));  title('V'); 
    xlabel('Seconds'); ylabel('ft/s');
    subplot(4,3,6);  plot(t,x(6,:));  title('W'); 
    xlabel('Seconds'); ylabel('ft/s');

    subplot(4,3,7);  plot(t,rad2deg(x(7,:)));  title('Phi');
    xlabel('Seconds'); ylabel('deg');
    subplot(4,3,8);  plot(t,rad2deg(x(8,:)));  title('Theta'); 
    xlabel('Seconds'); ylabel('deg');
    subplot(4,3,9);  plot(t,rad2deg(x(9,:)));  title('Psi');
    xlabel('Seconds'); ylabel('deg');

    subplot(4,3,10); plot(t,rad2deg(x(10,:))); title('P'); 
    xlabel('Seconds'); ylabel('deg/s');
    subplot(4,3,11); plot(t,rad2deg(x(11,:))); title('Q'); 
    xlabel('Seconds'); ylabel('deg/s');
    subplot(4,3,12); plot(t,rad2deg(x(12,:))); title('R');
    xlabel('Seconds'); ylabel('deg/s');
    
end


%     %% Output runtime stats to command window
%     END = toc(START);
%     fprintf(1,'Runtime: %f\n',END);
%     fprintf(1,'Percent realtime: %f\n',(sec/END)*100);
%     %% Build Vt, alfa, beta from U,V,W & Build GAMMA KI
%     VT = sqrt(x(4,:).^2 + x(5,:).^2 + x(6,:).^2 );
%     ALFA = rad2deg(atan(x(6,:)./x(4,:)));
%     BETA = rad2deg(atan(x(5,:)./sqrt(x(4,:).^2+x(6,:).^2)));
%     GAMMA = rad2deg(x(8,:)) - ALFA;
%     KI = rad2deg(atan2(x(2,:),x(1,:)));   
%     %% Produce plots / debug output.
%     figure(1);
%     subplot(4,3,1);  plot(t,x(1,:));  title('X'); 
%     xlabel('Seconds'); ylabel('ft');
%     subplot(4,3,2);  plot(t,x(2,:));  title('Y');
%     xlabel('Seconds'); ylabel('ft');
%     subplot(4,3,3);  plot(t,-x(3,:));  title('-Z');
%     xlabel('Seconds'); ylabel('Altitude');
% 
%     subplot(4,3,4);  plot(t,x(4,:));  title('U');
%     xlabel('Seconds'); ylabel('ft/s');
%     subplot(4,3,5);  plot(t,x(5,:));  title('V'); 
%     xlabel('Seconds'); ylabel('ft/s');
%     subplot(4,3,6);  plot(t,x(6,:));  title('W'); 
%     xlabel('Seconds'); ylabel('ft/s');
% 
%     subplot(4,3,7);  plot(t,rad2deg(x(7,:)));  title('Phi');
%     xlabel('Seconds'); ylabel('deg');
%     subplot(4,3,8);  plot(t,rad2deg(x(8,:)));  title('Theta'); 
%     xlabel('Seconds'); ylabel('deg');
%     subplot(4,3,9);  plot(t,rad2deg(x(9,:)));  title('Psi');
%     xlabel('Seconds'); ylabel('deg');
% 
%     subplot(4,3,10); plot(t,rad2deg(x(10,:))); title('P'); 
%     xlabel('Seconds'); ylabel('deg/s');
%     subplot(4,3,11); plot(t,rad2deg(x(11,:))); title('Q'); 
%     xlabel('Seconds'); ylabel('deg/s');
%     subplot(4,3,12); plot(t,rad2deg(x(12,:))); title('R');
%     xlabel('Seconds'); ylabel('deg/s');
%     
%     figure(2);
%     subplot(3,2,1);  plot(t,VT);    title('Vt'); 
%     xlabel('Seconds'); ylabel('ft/s');
%     subplot(3,2,2);  plot(t,ALFA);  title('Alfa'); 
%     xlabel('Seconds'); ylabel('deg');
%     subplot(3,2,3);  plot(t,BETA);  title('Beta'); 
%     xlabel('Seconds'); ylabel('deg');
% 
%     subplot(3,2,4);  plot(t,GAMMA);  title('Gamma'); 
%     xlabel('Seconds'); ylabel('deg');
%     subplot(3,2,5);  plot(t,KI);     title('Ki');
%     xlabel('Seconds'); ylabel('deg');   

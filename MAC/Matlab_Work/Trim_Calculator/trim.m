%% Author: Joshua Reed, MAE 744
% Function trim( )
% Calculates delH, Alfa, and Throttle required for steady level flight

function [Alfa delH Thtl] = trim(alt, vt)
%     close all; %format long e; format compact;
    %%% Add mfiles directory to path (Lets me keep my source files
    %%% organized)
    addPath = sprintf('%s%s',pwd,'/Data');
    path(addPath,path);
    addPath = sprintf('%s%s',pwd,'/mfiles');
    path(addPath,path);
    addPath = sprintf('%s%s',pwd,'/initialization');
    path(addPath,path);
    %% Load Data Dependencies
    declare_global;
    loaddata;
    %% Initialize for trim calc;
  
    aero.Vt      = vt;              % TAS Velocity (ft/s)
    aero.alfa    = 0;               % AoA Degrees
    aero.beta    = 0;              % Sideslipt degrees
    aero.beta_rad= deg2rad(0);     % Sideslip radians
    aero.phi     = 0;     % Roll angle
    aero.th      = 0;      % Pitch Angle
    aero.psi     = 0;    % Yaw Angle
    aero.P       = deg2rad(0);     % Roll Rate  (rad/s)
    aero.Q       = deg2rad(0);      % Pitch Rate (rad/s)
    aero.R       = deg2rad(0);     % Yaw Rate   (rad/s)
    aero.X       =  0;        % X inertial position
    aero.Y       = 0;        % Y inertial position
    aero.Z       = alt;           % Z inertial position (pos toward ground)
    aero.H       = -1*aero.Z;        % Altitude (ft)
    % Initialize altitude dependent variables to 1;
    aero.Temp    = 1;                % OAT due to alt
    aero.Pres    = 1;                % pressure due to alt
    aero.rho     = 1;                % rho due to alt
    aero.Vs      = 1;                % Sonic speed due to altitude
    % Initialize all other variables
    aero.T       = 1;                % Thrust
    aero.fax     = 1;
    aero.fay     = 1;
    aero.faz     = 1;
    aero.la      = 1;
    aero.ma      = 1;
    aero.na      = 1;
    aero.fpx     = 1;
    aero.fpy     = 0;
    aero.fpz     = 0;
    aero.lp      = 0;
    aero.mp      = 0;
    aero.np      = 0;
    aero.gx      = 1;
    aero.gy      = 1;
    aero.gz      = 1;
    aero.U = aero.Vt*cosd(aero.alfa)*cosd(aero.beta);
    aero.V = aero.Vt*sind(aero.beta);
    aero.W = aero.Vt*sind(aero.alfa)*cosd(aero.beta);
    % Define Control Input Parameters (Degrees)
    ctrlin.delH        = 0;      % Horizontal tail deflection (Pos down)
    ctrlin.delA        = 0;       % Aileron Deflection       (Right down Pos)
    ctrlin.delR        = 0;       % Rudder Defection          (Left is Pos)
    ctrlin.delFlap     = 0;       % Flap Defection            (Always Pos)
    ctrlin.delSpBr     = 0;        % Speedbrake Deflection     (Always Pos)
    ctrlin.thtl        = 0.5;     % Trottle setting  (100% max)
    
    % Get Trim values
    f_trim = calc_statics();
    % Initialize Data Structures with trim values.
    [aero ctrlin] = init_steady_level(f_trim);
    %% Define Initial State space vectors & Constants.
    Alfa = aero.alfa;
    delH = ctrlin.delH;
    Thtl = ctrlin.thtl;
end

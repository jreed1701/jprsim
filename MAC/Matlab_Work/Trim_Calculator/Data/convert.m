function convert(arrayname,array, dim)

inputfile = 'result.txt';
formatspec = '%1.4f';

FILE = fopen(inputfile,'w');

if(dim == 1) % Row vector
    len = length(array);
    lens = num2str(len);
    fprintf(FILE,'double %s [%s] = {\r\n', arrayname, lens);
    
    for i=1:len
        if(i == len)
           fprintf(FILE,'%s',num2str(array(i),formatspec));
        else
           fprintf(FILE,'%s, ',num2str(array(i),formatspec));
        end
    end
    fprintf(FILE,'\r\n');
    fprintf(FILE,'};');
    
else % 2-d matrix
    
    lenx = length(array(:,1));
    leny = length(array(1,:));
    
    fprintf(FILE,'double %s [%s] [%s] = {\r\n', arrayname, num2str(lenx), num2str(leny));
    
    for i=1:lenx
        fprintf(FILE,'{');
        for j=1:leny
            if(j == leny)
                fprintf(FILE,'%s',num2str(array(i,j),formatspec));
            else
                fprintf(FILE,'%s, ',num2str(array(i,j),formatspec));
            end
        end
        if(i == lenx)
            fprintf(FILE,'}');
        else
            fprintf(FILE,'},\r\n');
        end
    end
    fprintf(FILE,'\r\n');
    fprintf(FILE,'};');
end
    fclose(FILE);
end
%% Author: Joshua Reed, MAE 744

% This script clears workspace and loads F-16 data.

%clc;
%clear all;

aero_dat_f16b;
geom_dat_f16b;
iner_dat_f16b;
prop_dat_f16b;
atmos_dat;
load_const;

function plotControls(filename)

% Order of variables in input file are as follows
% 1 time
% 2 alfa   8 v        14 q
% 3 beta   9 w        15 r
% 4 x      10 phi
% 5 y      11 theta
% 6 z      12 psi
% 7 u      13 p

load(filename);
data = JPRSIM_LOG;

t       = data(:,1);
alfa    = data(:,2);
beta    = data(:,3);
x       = data(:,4);
y       = data(:,5);
z       = data(:,6);
u       = data(:,7);
v       = data(:,8);
w       = data(:,9);
phi     = data(:,10);
theta   = data(:,11);
psi     = data(:,12);
p       = data(:,13);
q       = data(:,14);
r       = data(:,15);
lat       = data(:,16);
lon       = data(:,17);
alt       = data(:,18);
%heading   = data(:,19);

delH = data(:,22);
delA = data(:,23);
delR = data(:,24);
Flap = data(:,25);
SpdBr= data(:,26);
Thtl = data(:,27);

figure(6);
title('Control Surface Position vs. Time');
subplot(6,1,1);
plot(t,delH);
ylabel('Elevator');
subplot(6,1,2);
plot(t,delA);
ylabel('Aileron');
subplot(6,1,3);
plot(t,delR);
ylabel('Rudder');
subplot(6,1,4);
plot(t,Flap);
ylabel('Flap');
subplot(6,1,5);
plot(t,SpdBr);
ylabel('Speed Brake');
subplot(6,1,6);
plot(t,Thtl);
ylabel('Throttle');
xlabel('Time (s)');




end
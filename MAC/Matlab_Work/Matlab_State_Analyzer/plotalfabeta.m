function plotalfabeta(filename)

% Order of variables in input file are as follows
% 1 time
% 2 alfa   8 v        14 q
% 3 beta   9 w        15 r
% 4 x      10 phi
% 5 y      11 theta
% 6 z      12 psi
% 7 u      13 p

load(filename);
data = JPRSIM_LOG;

t       = data(:,1);
alfa    = data(:,2);
beta    = data(:,3);
x       = data(:,4);
y       = data(:,5);
z       = data(:,6);
u       = data(:,7);
v       = data(:,8);
w       = data(:,9);
phi     = data(:,10);
theta   = data(:,11);
psi     = data(:,12);
p       = data(:,13);
q       = data(:,14);
r       = data(:,15);

% figure(1);
% subplot(3,4,1);
% plot(t,x);
% title('x');
% ylabel('m');
% subplot(3,4,2);
% plot(t,y);
% title('y');
% ylabel('m');
% subplot(3,4,3);
% plot(t,z);
% title('z');
% ylabel('m');
% subplot(3,4,4);
% plot(t,u);
% title('u');
% ylabel('ft/s');
% subplot(3,4,5);
% plot(t,v);
% title('v');
% ylabel('ft/s');
% subplot(3,4,6);
% plot(t,w);
% title('w');
% ylabel('ft/s');
% subplot(3,4,7);
% plot(t,rad2deg(phi));
% title('phi');
% ylabel('deg');
% subplot(3,4,8);
% plot(t,rad2deg(theta));
% title('theta');
% ylabel('deg');
% subplot(3,4,9);
% plot(t,rad2deg(psi));
% title('psi');
% ylabel('deg');
% subplot(3,4,10);
% plot(t,p);
% title('p');
% ylabel('rad/s');
% subplot(3,4,11);
% plot(t,q);
% title('q');
% ylabel('rad/s');
% subplot(3,4,12);
% plot(t,r);
% title('r');
% ylabel('rad/s');

figure(2)
subplot(1,2,1);
plot(t,rad2deg(alfa));
title('alfa');
xlabel('rad/s');
subplot(1,2,2);
plot(t,rad2deg(beta));
title('beta');
xlabel('rad/s');

end
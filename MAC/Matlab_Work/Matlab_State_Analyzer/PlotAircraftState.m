function varargout = PlotAircraftState(varargin)
% PLOTAIRCRAFTSTATE MATLAB code for PlotAircraftState.fig
%      PLOTAIRCRAFTSTATE, by itself, creates a new PLOTAIRCRAFTSTATE or raises the existing
%      singleton*.
%
%      H = PLOTAIRCRAFTSTATE returns the handle to a new PLOTAIRCRAFTSTATE or the handle to
%      the existing singleton*.
%
%      PLOTAIRCRAFTSTATE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTAIRCRAFTSTATE.M with the given input arguments.
%
%      PLOTAIRCRAFTSTATE('Property','Value',...) creates a new PLOTAIRCRAFTSTATE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotAircraftState_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotAircraftState_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotAircraftState

% Last Modified by GUIDE v2.5 17-Aug-2013 18:04:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PlotAircraftState_OpeningFcn, ...
                   'gui_OutputFcn',  @PlotAircraftState_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlotAircraftState is made visible.
function PlotAircraftState_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotAircraftState (see VARARGIN)

% Choose default command line output for PlotAircraftState
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlotAircraftState wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PlotAircraftState_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
    addpaths;
    plotData('JPRSIM_LOG.txt');

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
    addpaths;
    plotalfabeta('JPRSIM_LOG.txt');
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
    addpaths;
    plotPosition('JPRSIM_LOG.txt');
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
    addpaths;
    plotNED('JPRSIM_LOG.txt');
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
    addpaths;
    plotLLA('JPRSIM_LOG.txt');
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
    addpaths;
    plotControls('JPRSIM_LOG.txt');
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

// InceptorParser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdlib.h>


int _tmain(int argc, _TCHAR* argv[])
{
					 //  pitch     roll       yaw       th1      th2     blu1    blu2     red
	char string[75] = "#0.152385#-0.859568#-0.45300#0.000000#0.500000#0.950000#0.850000#0.125687\0";
	bool num = false;
	int pos = -1;
	char* ptr;
	double val[8];

	for(int i = 0; i < 75; i++)
	{
		if( string[i] == '#' && !num)
		{
			num = true;
			pos++;
		}

		// if found the start of a number
		if(num)
		{
			ptr = &string[i+1];
			// check if number is negative
			if( string[i+1] == '-')
			{
				// take 9 chars 
				char temp[9];
				memcpy(temp,ptr,9);
				val[pos] = atof(temp);
			}
			else
			{
				// take next 8 chars
				char temp[8];
				memcpy(temp,ptr,8);
				val[pos] = atof(temp);
			}
			num = false;
		}

	}

	for(int i = 0; i < 8; i++)
	{
		printf("Value of item #%d is %f \n",(i+1),val[i]);
	}

	system("pause");

	return 0;
}


#include <math.h>

#include "constants.h"


#define PI 3.141592653589793238462

#define a   6378137.0
#define f   (1.0 / 298.257223563 )
#define e2  ( (2 * f) - ( f * f ) )
#define b   (a * (1 - f ) )
#define ep2 (f * (2 - f) ) / ( (1 - f) * ( 1 - f) )



//#define MAJA   (6378137.0)             // semi major axis of ref ellipsoid 
//#define FLAT   (1.0/298.2572235630)    // flattening coef of ref ellipsoid. 
////  These are derived values from the above WGS-84 values 
//#define ESQR   (FLAT * (2.0-FLAT))     // 1st eccentricity squared 
//#define OMES   (1.0 - ESQR)            // 1 minus eccentricity squared 
//#define EFOR   (ESQR * ESQR)           // Sqr of the 1st eccentricity squared 
//#define ASQR   (MAJA * MAJA)           // semi major axis squared = nlmaja** 

struct ENU{
	double E;
	double N;
	double U;
};

struct ECEF{
	double X;
	double Y;
	double Z;
};

struct GEO{
	double LAT;
	double LON;
	double ALT;
};

// Function Prototypes
double METERS_DEGLON(double x);
double METERS_DEGLAT(double x);

//Position Conversion functions 

// Convert lat/lon/height to Earth Centered Earth fixed
ECEF llh2xyz(double lat0, double lon0, double h)
{
	ECEF loc;
	double lat = lat0 * D2R;
	double lon = lon0 * D2R;

	double chi = sqrt( 1 - (e2* pow(sin(lat),2.0)));

	loc.X = ( (a/chi) + h) * cos(lat) * cos(lon);
	loc.Y = ( (a/chi) + h) * cos(lat) * sin(lon);
	loc.Z = ( ((a*(1-e2))/chi) + h ) * sin(lat);

	return loc;
}

// Convert Earth Centered Earth Fixed into East/North/Down coordinates
ENU xyz2enu(ECEF ref, ECEF loc)
{
	ENU cords;

	double phi = atan2(ref.Z, sqrt(pow(ref.X,2.0) + pow(ref.Y,2.0)) );
	double lambda = atan2(ref.Y,ref.X);

	cords.E = (-1.0*sin(lambda) * (loc.X - ref.X)) 
		        + ( cos(lambda) * (loc.Y - ref.Y));

	cords.N = (-1.0*sin(phi) * cos(lambda) * (loc.X-ref.X) )
		        - ( sin(phi) * sin(lambda) * (loc.Y-ref.Y) )
		        + ( cos(phi)               * (loc.Z-ref.Z)); 

	cords.U = ( cos(phi) * cos(lambda) * (loc.X-ref.X) )
		   +  ( cos(phi) * sin(lambda) * (loc.Y-ref.Y) )
		   +  ( sin(phi) *               (loc.Z-ref.Z) );

	return cords;
}

ECEF enu2xyz(double latref, double lonref, double refH, ENU loc)
{
	ECEF cords, temp;

	temp = llh2xyz(latref, lonref, refH);
	double phi = atan2(temp.Z, sqrt(pow(temp.X,2.0) + pow(temp.Y,2.0)) );

	cords.X = ( -1.0 * sin(latref)*loc.E ) - ( cos(lonref)*sin(phi)*loc.N) + (cos(lonref)*cos(phi)*loc.U) + temp.X;
	cords.Y = (        cos(lonref)*loc.E ) - ( sin(lonref)*sin(phi)*loc.N) + (cos(phi)*sin(lonref)*loc.U) + temp.Y;
    
	cords.Z = cos(phi)*loc.N + sin(phi)*loc.U + temp.Z;

	return cords;
}

GEO xyz2llh( ECEF loc)
{
  GEO pos;
  
  double X = loc.X;
  double Y = loc.Y;
  double Z = loc.Z;
	
  double r2 = pow(X,2.0) + pow(Y,2.0);
  double r  = sqrt(r2);
  double E2 = pow(a,2.0) - pow(b,2.0);
  double F  = 54 * pow(b,2.0) * pow(Z,2.0);
  double G  = r2 + (1-e2)*pow(Z,2.0) - e2*E2;
  double c = (e2*e2*F*r2) /(G*G*G);
  double s =  1.0 + c + pow( (sqrt(c*c + 2.0*c) ),(1/3));
  double P = F / (3.0*pow((s+1.0/s+1.0),2.0)*G*G);
  double Q = sqrt(1+2.0*e2*e2*P);
  double ro = -1.0*(e2*P*r)/(1+Q) + sqrt((a*a/2)*(1+1./Q) - (pow((1-e2)*P*Z,2.0))/(Q*(1+Q)) - P*(r2/2));
  double tmp = pow((r - e2*ro),2.0);
  double U   = sqrt( tmp + pow(Z,2.0) );
  double V = sqrt( tmp + (1.0 - e2)*pow(Z,2.0) );
  double zo = (pow(b,2.0)*Z)/(a*V);
 
  pos.ALT = U*( 1.0 - pow(b,2.0)/(a*V));
  pos.LAT = atan( (Z + ep2*zo)/r );
  pos.LON = atan2(Y,X);

  return pos;
}

GEO XY2NED(double x, double y, double z, double LAT0, double LON0){
	// Assumes no rotation offset, no rotation angle, no rms error
	// xyz comes in as feet, LAT0 LON0 comes in as degrees
	GEO Local;
	Local.ALT = z; //pass through altitude as z. no change
	Local.LON = LON0 + ( x / METERS_DEGLON(LAT0));
	Local.LAT = LAT0 + ( y / METERS_DEGLAT(LAT0));

	return Local; // returns z in feet, LAT and LON in degrees.
}

GEO doConversion(GEO refPos, GEO curPos, ECEF travel)
{
	GEO newPos;
	ECEF ref, cur, newECEF;
	ENU enuPos;

	ref = llh2xyz(refPos.LAT, refPos.LON, refPos.ALT);
	cur = llh2xyz(curPos.LAT, curPos.LON, curPos.ALT);

	enuPos = xyz2enu(ref, cur);

	// increment according to what the FDM calculated in ENU

	// Feedback to get new lat lon alt
	newECEF = enu2xyz(refPos.LAT, refPos.LON, refPos.ALT, enuPos);

	newPos = xyz2llh( newECEF );

	return newPos;
}

double METERS_DEGLON(double x){
	return ((111415.13*cos(x*D2R))-(94.55*cos(3.0*x*D2R))+(0.12*cos(5.0*x*D2R)));
}

double METERS_DEGLAT(double x){
	return (111132.09 - (566.05*cos(2.0*x*D2R))+(1.2*cos(4.0*x*D2R))-(0.002*cos(6.0*x*D2R)));
}
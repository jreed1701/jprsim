/*jprsim.cpp : Defines the entry point for the console application.
Author: Joshua Reed
Date: 2/10/2013
Description: Custom C code for an F-16 flight simulator
*/

#include "Stdafx.h"

// Built in Headers
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <iostream>

// Custom header files
#include "interp.h"
#include "jprsim.h"
#include "constants.h"

// Flight Gear Header(s)
//#include "net_fdm.hxx"

#define STEP	0.1     	// 5 Hz
#define UPDATE_PERIOD 100
#define RUNTIME 10			// seconds

// Create objects needed to support UDP transmission
// IP and port where FG is listening
//struct sockaddr_in echoServAddr;
//char* fg_ip = "127.0.0.1";
//int   fg_port = 5500;
//int   fg_socket = 0;

// Control Inceptor UDP
//char* localhost = "127.0.0.1";
//int inceptor_port = 5505;
//int inceptor_socket = 0;

// Global Variables
X X0, curX;
U U0, curU;
data forces;
GEO refPos, curPos;
ECEF travel;

double curLAT = 0.0;
double curLON = 0.0;

double*** cax_alfa_beta_hrzt_tab;
double*** caz_alfa_beta_hrzt_tab;
double*** cal_alfa_beta_hrzt_tab;
double*** cam_alfa_beta_hrzt_tab;
double*** can_alfa_beta_hrzt_tab;
double*** thst_alt_mach_powr_tab;

void write_data(FILE *fp, X x, GEO curPos, double t);

using namespace std;

int main(int argc, char* argv[])
{
	// Simulation variables
	FGNetFDM fdm;
	memset(&fdm,0,sizeof(fdm));

	double time = 0.0;

	bool enableRK4 = true;

	FILE *fid;
	//fid = fopen("C:\Users\Joshua\Documents\MATLAB\Thesis Work\JPRSIM_LOG.txt","w");
	fid = fopen("JPRSIM_LOG.txt","w");

	// Initialize all simulation resources and initial conditions
	initializeSimulation();

	while(time < RUNTIME){

		// Sleep for step interval
		Sleep(UPDATE_PERIOD);

		if(time > 0.1)
		{
			U0.delH = -7.1076;
		}

		// Update inceptor positions
		//curU = proccessInceptors(inceptor_socket, inceptor_port);

		if( enableRK4)
		{
			// Calculate RK4
			curX = RK4(curX,U0,STEP,forces,cax_alfa_beta_hrzt_tab,caz_alfa_beta_hrzt_tab,cal_alfa_beta_hrzt_tab,
			cam_alfa_beta_hrzt_tab,can_alfa_beta_hrzt_tab,thst_alt_mach_powr_tab);

			// Convert Inertial position to Geodetic coordinates.
			curX.alfa = 0;
			curX.beta = 0;
	
			travel.X = curX.x;
			travel.Y = curX.y;
			travel.Z = -1.0*curX.z;
			curPos = doConversion(refPos, curPos, travel);
		}
		//fwrite("&f %f",time,curX.alfa curX. );

		// Export aircraft state and position to FlightGear
		//export2FG(X0, curU, curPosition, fg_socket, fg_ip, fg_port, &fdm);

		// Log data
		write_data(fid,curX, curPos, time);

		// Increment time step
		time += STEP;
	}
	fclose(fid);
	printf("Exited with time = %f\n",time);
	return EXIT_SUCCESS;
}

void write_data(FILE *fp, X x, GEO pos, double t){
	fprintf(fp,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",t,x.alfa, x.beta, x.x, x.y, x.z, x.u, x.v, x.w, x.phi, x.theta, x.psi, x.p, x.q, x.r, pos.LAT, pos.LON, pos.ALT, heading);	
}

void initializeSimulation()
{
	//fg_socket = socketUDP_client_init();
	//printf("FG socket descriptor is: %d\n", fg_socket);

	//inceptor_socket = socketUDP_init(localhost, inceptor_port);
	//printf("Inceptor Socket descriptor is: %d\n", inceptor_socket);



	double LAT0 = 37.6392;					// Initial lattitude in radians
	double LON0 = -122.4195;				// Initial Logitude  in radians
	double ALT0 = 25000*ft2m;     			// Initial alititude in meters

	refPos.LAT = LAT0 * D2R;
	refPos.LON = LON0 * D2R;
	refPos.ALT = ALT0 * m2ft;

	// initialize all 3d tables
	cax_alfa_beta_hrzt_tab = initialize5(cax_alfa_beta_hrzt_m25_tab, cax_alfa_beta_hrzt_m10_tab, cax_alfa_beta_hrzt_0_tab, 
		cax_alfa_beta_hrzt_p10_tab, cax_alfa_beta_hrzt_p25_tab);
	caz_alfa_beta_hrzt_tab = initialize5(caz_alfa_beta_hrzt_m25_tab, caz_alfa_beta_hrzt_m10_tab, caz_alfa_beta_hrzt_0_tab,
		caz_alfa_beta_hrzt_p10_tab, caz_alfa_beta_hrzt_p25_tab);
	cal_alfa_beta_hrzt_tab = initialize3(cal_alfa_beta_hrzt_m25_tab, cal_alfa_beta_hrzt_0_tab, cal_alfa_beta_hrzt_p25_tab);
	cam_alfa_beta_hrzt_tab = initialize5(cam_alfa_beta_hrzt_m25_tab, cam_alfa_beta_hrzt_m10_tab, cam_alfa_beta_hrzt_0_tab,
		cam_alfa_beta_hrzt_p10_tab, cam_alfa_beta_hrzt_p25_tab);
	can_alfa_beta_hrzt_tab = initialize3(can_alfa_beta_hrzt_m25_tab, can_alfa_beta_hrzt_0_tab, can_alfa_beta_hrzt_p25_tab);
	thst_alt_mach_powr_tab = initialize3Prop(thst_alt_mach_powr_idle_tab, thst_alt_mach_powr_mil_tab, thst_alt_mach_powr_max_tab);

	// any other initializations required
	forces.delta   = (ixx*iyy*izz)-(ixx*pow(iyz,2.0))-(iyy*pow(izx,2.0))-(izz*pow(ixy,2.0))-(2.0*ixy*iyz*izx);
	forces.ixx_inv = (iyy*izz-pow(iyz,2.0))/forces.delta;
	forces.ixy_inv = (izz*ixy+izx*iyz)/forces.delta; //% neg
	forces.iyy_inv = (ixx*izz-pow(izx,2.0))/forces.delta;
	forces.iyz_inv = (ixx*iyz+ixy*izx)/forces.delta; //% neg
	forces.izz_inv = (ixx*iyy-pow(ixy,2.0))/forces.delta;
	forces.izx_inv = (iyy*izx+ixy*iyz)/forces.delta; //% neg

	X0.x = 0.0;
	X0.y = 0.0;
	X0.z = -1.0*ALT0*m2ft;
	X0.vt = 600.0;
	X0.alfa = 3.4147;    // Degrees
	X0.beta = 0.0;		 // Degrees
	X0.u = X0.vt*cos(X0.alfa*D2R)*cos(X0.beta*D2R);
	X0.v = X0.vt*sin(X0.beta*D2R);
	X0.w = X0.vt*sin(X0.alfa*D2R)*cos(X0.beta*D2R);
	X0.phi = 0.0;
	X0.theta = 0.0;
	X0.psi = 0.0*D2R;
	X0.p = 0.0;
	X0.q =0.0;
	X0.r = 0.0;


	// initial input trim for steady level flight
	U0.delH = -6.1076;
	U0.delA = 0.0;
	U0.delR = 0.0;
	U0.Flap = 0.0;
	U0.SpdBr = 0.0;
	U0.Th = 0.2713;

	curX = X0; // Set RK4 initial state.
	curLAT = LAT0;
	curLON = LON0;
}

	/* Test iterpolation files. // All interp routines work
	cax_alfa_spbr     = lookup1d(1,20,alfa_aero_tab,cax_alfa_spbr_tab);
	cax_alfa_beta_flaple = lookup2d(2,1,14,19,alfa_aero_flaple_tab,beta_aero_tab,cax_alfa_beta_flaple_tab); // most of the 2d arrays are 19 collums with one at 7 and three at 8.
	cax_alfa_beta_0hrzt  = lookup3d(2,2,0,20,19,5,alfa_aero_tab,beta_aero_tab, hrzt_aero_long_tab,cax_alfa_beta_hrzt_tab);
	*/
//force = getForces(force,state,input,cax_alfa_beta_hrzt_tab,caz_alfa_beta_hrzt_tab,
	//	cal_alfa_beta_hrzt_tab,cam_alfa_beta_hrzt_tab,can_alfa_beta_hrzt_tab); // works

	//envars = getEnvars(state.z, state.vt);//works
	//force.fpx = getFPX(state.z,state.vt,input.Th,envars,thst_alt_mach_powr_tab); // slightly off...

	//state = getEOM(state,input,envars,force);

	/*
	printf("cax: %f\n",force.cax);
	printf("cay: %f\n",force.cay);
	printf("caz: %f\n",force.caz);
	printf("cal: %f\n",force.cal);
	printf("cam: %f\n",force.cam);
	printf("can: %f\n",force.can);
	*/
	//printf("xdot: %f\n",state.x);
	//printf("ydot: %f\n",state.y);
	//printf("zdot: %f\n",state.z);
	//printf("udot: %f\n",state.u);
	//printf("vdot: %f\n",state.v);
	//printf("wdot: %f\n",state.w);
	//printf("phidot: %f\n",state.phi);
	//printf("thetadot: %f\n",state.theta);
	//printf("psidot: %f\n",state.psi);
	//printf("pdot: %f\n",state.p);
	//printf("qdot: %f\n",state.q);
	//printf("rdot: %f\n",state.r);

	// Vt,alfa,beta do not get updated yet...  and lots of pain...	

	//curX = getEOM(X0,U0,forces,cax_alfa_beta_hrzt_tab,caz_alfa_beta_hrzt_tab,cal_alfa_beta_hrzt_tab,
	//	cam_alfa_beta_hrzt_tab,can_alfa_beta_hrzt_tab,thst_alt_mach_powr_tab);


/* STEADY LEVEL FLIGHT TRIM CASE
double LAT0 = 37.6392;					// Initial lattitude in radians
double LON0 = -122.4195;				// Initial Logitude  in radians
double ALT0 = 25000*ft2m;     			// Initial alititude in meters

X0.x = 0.0;
X0.y = 0.0;
X0.z = -1.0*ALT0*m2ft;
X0.vt = 600.0;
X0.alfa = 3.4147;
X0.beta = 0.0;
X0.u = X0.vt*cos(X0.alfa*D2R)*cos(X0.beta*D2R);
X0.v = X0.vt*sin(X0.beta*D2R);
X0.w = X0.vt*sin(X0.alfa*D2R)*cos(X0.beta*D2R);
X0.phi = 0.0;
X0.theta = 0.0;
X0.psi = 0.0*D2R;
X0.p = 0.0;
X0.q =0.0;
X0.r = 0.0;


// initial input trim for steady level flight
U0.delH = -6.1076;
U0.delA = 0.0;
U0.delR = 0.0;
U0.Flap = 0.0;
U0.SpdBr = 0.0;
U0.Th = 0.2713;
*/